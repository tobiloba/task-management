/* eslint-disable @typescript-eslint/camelcase */
import { Model } from 'objection';
import { TaskKnex } from '../knex';

const TaskModel = Model;
TaskModel.knex(TaskKnex);

export class BaseModel extends TaskModel {
  created_at?: Date;
  updated_at?: Date;

  $beforeInsert() {
    this.created_at = new Date();
    this.updated_at = new Date();
  }

  $beforeUpdate() {
    this.updated_at = new Date();
  }
}
