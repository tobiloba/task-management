import { UserRepository } from './userRepository';
export class UserService {
  static async addUser(params: any) {
    return await UserRepository.addUser(params);
  }
  static async getUser() {
    return await UserRepository.getUser();
  }
}
