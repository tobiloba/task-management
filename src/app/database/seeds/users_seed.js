/* eslint-disable @typescript-eslint/camelcase */
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { runSeed } = require('../runSeed');

const usersData = [
  {
    id: 1,
    first_name: 'Alexa',
    last_name: 'tee',
    role: 'PRO',
    password: 'password', // Default Password: password
  },
  {
    id: 2,
    first_name: 'Lignt',
    last_name: 'Source',
    role: 'President',
    password: 'password', // Default Password: password
  },
];
exports.seed = runSeed('users', usersData);
