/* eslint-disable @typescript-eslint/camelcase */
import { BaseModel } from './BaseModel';
import { Task } from './Task';
export class User extends BaseModel {
  static tableName = 'users';

  readonly id?: number;
  first_name: string | undefined;
  last_name: string | undefined;
  role: string | undefined;

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['first_name', 'last_name', 'role'],
      properties: {
        id: { type: 'number' },
        first_name: { type: 'string' },
        last_name: { type: 'string' },
        role: { type: 'string' },
      },
    };
  }

  static relationMappings = {
    tasks: {
      relation: BaseModel.HasManyRelation,
      modelClass: `${__dirname}/Task`,
      join: {
        from: 'users.id',
        to: 'tasks.assign_id',
      },
    },
  };
}
