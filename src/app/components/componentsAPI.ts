import { userAPI } from './authentication/userAPI';
import { taskAPI } from './manageTask/taskAPI';

export const componentsAPI = [userAPI, taskAPI];
