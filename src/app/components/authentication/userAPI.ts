import express from 'express';
import { UserController } from './userController';
const app = express.Router();

app.post('/user', async (req, res) => {
  const { message, data } = await UserController.addUser(req.body);
  res.json({
    status: true,
    message,
    data: data,
  });
});
app.get('/users', async (req, res) => {
  const { message, data } = await UserController.getUser();
  res.json({
    status: true,
    message,
    data: data,
  });
});

export const userAPI = app;
