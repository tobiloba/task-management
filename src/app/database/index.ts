export * from './knex';
export * from './models';

import { TaskKnex } from './knex';

export { transaction } from 'objection';

export const TaskDb = TaskKnex;
