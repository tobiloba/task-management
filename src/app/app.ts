import express from 'express';
import path from 'path';
import httpContext from 'express-http-context';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';
import helmet from 'helmet';

import compression from 'compression';

import { componentsAPI } from './components';

class App {
  public express: express.Application;

  constructor() {
    this.express = express();
    this.init();
  }

  private init(): void {
    /**
     * init necessary middlewares here
     */
    this.express.use(bodyParser.urlencoded({ limit: '2mb', extended: true }));
    this.express.use(bodyParser.json({ limit: '10mb' }));
    this.express.use(cors());
    this.express.use(helmet());
    this.express.use(compression());
    this.express.use(morgan('dev'));

    /**
     * auth routes here
     */
    this.express.use('/api/v1', componentsAPI);

    /**
     * init OAuth2 authenticate middleware and set user in context
     */

    /**
     * init http context middleware arrgh. should always be bootsrapped
     * afer oauthserver. it did me strong thing Pele.
     */
    // this.express.use(httpContext.middleware);

    /**
     * set user on request middleware here
     */
    /**
     * accessible routes here
     */

    /**handle 404 not found endpoints
     * handle all errors || exceptions gracefully hopefully arrgh
     */
  }
}

export default new App().express;
