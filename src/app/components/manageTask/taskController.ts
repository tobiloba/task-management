import { TaskService } from './taskService';
export class TaskController {
  static async addTask(params: any) {
    const data = await TaskService.addTask(params);
    return {
      message: 'Task Added Successfully',
      data,
    };
  }
  static async getTask() {
    const data = await TaskService.getTask();
    return {
      message: 'Task Fetched Successfully',
      data,
    };
  }
  static async changeTaskStatus(params: any) {
    const data = await TaskService.changeTaskStatus(params);
    return {
      message: 'Task Status Updated Successfully',
      data,
    };
  }
  static async deleteTask(params: any) {
    const data = await TaskService.deleteTask(params);
    return {
      message: 'Task Deleted Successfully',
      data,
    };
  }
  static async addComment(params: any) {
    const data = await TaskService.addComment(params);
    return {
      message: 'Comment Added Successfully',
      data,
    };
  }
  static async getComment() {
    const data = await TaskService.getComment();
    return {
      message: 'Comment Fetched Successfully',
      data,
    };
  }
}
