/* eslint-disable @typescript-eslint/camelcase */
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { runSeed } = require('../runSeed');

const taskData = [
  {
    id: 1,
    task: 'Person1',
    completion_date: new Date(),
    status: 'pending',
    priority: 'high',
    assign_to: 1,
  },
  {
    id: 2,
    task: 'Person2',
    completion_date: new Date(),
    status: 'pending',
    priority: 'low',
    assign_to: 2,
  },
  {
    id: 3,
    task: 'Person3',
    completion_date: new Date(),
    status: 'pending',
    priority: 'medium',
    assign_to: 3,
  },
  {
    id: 4,
    task: 'Person4',
    completion_date: new Date(),
    status: 'ongoing',
    priority: 'high',
    assign_to: 1,
  },
  {
    id: 5,
    task: 'Person5',
    completion_date: new Date(),
    status: 'completed',
    priority: 'low',
    assign_to: 2,
  },
  {
    id: 6,
    task: 'Person6',
    completion_date: new Date(),
    status: 'completed',
    priority: 'medium',
    assign_to: 3,
  },
];
exports.seed = runSeed('tasks', taskData);
