import { UserService } from './userService';
export class UserController {
  static async addUser(params: any) {
    const data = await UserService.addUser(params);
    return {
      message: 'User Added Successfully',
      data,
    };
  }
  static async getUser() {
    const data = await UserService.getUser();
    return {
      message: 'User Fetched Successfully',
      data,
    };
  }
}
