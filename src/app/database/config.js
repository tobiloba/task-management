/* eslint-disable @typescript-eslint/no-var-requires */
const dotenv = require('dotenv');
dotenv.config();

const get = name => process.env[name];

const config = {
  username: get('DATABASE_USERNAME'),
  password: get('DATABASE_PASSWORD'),
  database: get('DATABASE_NAME'),
  host: get('DATABASE_HOST'),
  port: get('DATABASE_PORT'),
  dialect: get('DATABASE_DIALECT'),
};

module.exports = {
  development: {
    ...config,
  },
  test: {
    ...config,
  },
  production: {
    ...config,
  },
};
