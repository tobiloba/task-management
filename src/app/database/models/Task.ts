/* eslint-disable @typescript-eslint/camelcase */
import { BaseModel } from './BaseModel';
import { User } from './User';
import { Comment } from './Comment';
export class Task extends BaseModel {
  static tableName = 'tasks';

  readonly id?: number;
  task: string | undefined;
  completion_date: Date | undefined;
  status: string | undefined;
  assign_to: number | undefined;
  priority: string | undefined;

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['task', 'completion_date', 'status', 'assign_to', 'priority'],
      properties: {
        id: { type: 'number' },
        task: { type: 'string' },
        completion_date: { type: 'Date' },
        status: { type: 'string' },
        assign_to: { type: 'number' },
        priority: { type: 'string' },
      },
    };
  }

  static relationMappings = {
    user: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: User,
      join: {
        from: 'users.id',
        to: 'tasks.assign_id',
      },
    },
    comments: {
      relation: BaseModel.HasManyRelation,
      modelClass: `${__dirname}/Comment`,
      join: {
        from: 'tasks.id',
        to: 'comments.task_id',
      },
    },
  };
}
