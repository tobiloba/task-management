import { Task, TaskKnex, Comment } from '../../database';
const TaskTable = Task.bindKnex(TaskKnex);

export class TaskRepository {
  static async addTask(params: any) {
    return TaskTable.query().insert(params);
  }

  static async getTask() {
    return TaskTable.query();
  }
  static async changeTaskStatus(params: any) {
    return TaskTable.query()
      .patch({ status: params.status })
      .where('id', '=', params.task_id);
  }
  static async deleteTask(params: any) {
    return TaskTable.query()
      .where('id', '=', params.id)
      .delete();
  }
  static async addCommentToTask(params: any) {
    return Comment.query().insert(params);
  }

  static async getComment() {
    return Comment.query().eager('task');
  }
}
