import express from 'express';
import { TaskController } from './taskController';
const app = express.Router();

app.post('/task', async (req, res) => {
  const { message, data } = await TaskController.addTask(req.body);
  res.json({
    status: true,
    message,
    data: data,
  });
});
app.get('/tasks', async (req, res) => {
  const { message, data } = await TaskController.getTask();
  res.json({
    status: true,
    message,
    data: data,
  });
});
app.put('/task/:task_id', async (req, res) => {
  const payload = Object.assign({}, req.body, req.params, req.query);
  const { message, data } = await TaskController.changeTaskStatus(payload);
  res.json({
    status: true,
    message,
    data: data,
  });
});
app.delete('/task/:task_id', async (req, res) => {
  const payload = Object.assign({}, req.body, req.params, req.query);
  const { message, data } = await TaskController.deleteTask(payload);
  res.json({
    status: true,
    message,
    data: data,
  });
});
app.post('/comment/:task_id', async (req, res) => {
  const payload = Object.assign({}, req.body, req.params, req.query);
  const { message, data } = await TaskController.addComment(payload);
  res.json({
    status: true,
    message,
    data: data,
  });
});
export const taskAPI = app;
