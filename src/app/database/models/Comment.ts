/* eslint-disable @typescript-eslint/camelcase */
import { BaseModel } from './BaseModel';
import { Task } from './Task';
export class Comment extends BaseModel {
  static tableName = 'comments';

  readonly id?: number;
  comment: string | undefined;
  task_id: number | undefined;

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['comment', 'task_id'],
      properties: {
        id: { type: 'number' },
        comment: { type: 'string' },
        task_id: { type: 'number' },
      },
    };
  }

  static relationMappings = {
    task: {
      relation: BaseModel.BelongsToOneRelation,
      modelClass: Task,
      join: {
        from: 'tasks.id',
        to: 'comments.task_id',
      },
    },
  };
}
