exports.up = function(knex, Promise) {
  return knex.schema.createTable('tasks', table => {
    table.charset('utf8mb4');
    table.collate('utf8mb4_unicode_ci');
    table.increments();
    table.string('task').notNullable();
    table.date('completion_date').notNullable();
    table
      .enu('status', ['pending', 'ongoing', 'completed'])
      .default('pending')
      .notNullable();
    table
      .enu('priority', ['high', 'medium', 'low'])
      .default('high')
      .notNullable();
    table
      .integer('assign_to')
      .unsigned()
      .notNullable();
    table.timestamps(true, true);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('tasks');
};
