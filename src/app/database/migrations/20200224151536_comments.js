exports.up = function(knex, Promise) {
  return knex.schema.createTable('comments', table => {
    table.charset('utf8mb4');
    table.collate('utf8mb4_unicode_ci');
    table.increments();
    table.string('comment').notNullable();
    table
      .integer('task_id')
      .unsigned()
      .notNullable();
    table.timestamps(true, true);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('comments');
};
