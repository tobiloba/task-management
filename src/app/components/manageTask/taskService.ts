/* eslint-disable @typescript-eslint/camelcase */
import { TaskRepository } from './taskRepository';
export class TaskService {
  static async addTask(params: any) {
    return await TaskRepository.addTask(params);
  }
  static async getTask() {
    return await TaskRepository.getTask();
  }

  static async addComment(params: any) {
    const payload = { ...params, task_id: Number(params.task_id) };

    return await TaskRepository.addCommentToTask(payload);
  }
  static async changeTaskStatus(params: any) {
    const payload = { ...params, task_id: Number(params.task_id) };

    return await TaskRepository.changeTaskStatus(payload);
  }
  static async deleteTask(params: any) {
    const payload = { ...params, id: Number(params.task_id) };

    return await TaskRepository.deleteTask(payload);
  }
  static async getComment() {
    return await TaskRepository.getComment();
  }
}
