import { User, TaskKnex } from '../../database';
const UserTable = User.bindKnex(TaskKnex);

export class UserRepository {
  static async addUser(params: any) {
    return UserTable.query().insert(params);
  }

  static async getUser() {
    return UserTable.query();
  }
}
